import React, { useEffect } from 'react';
import AppLoading from 'expo-app-loading';
import {
  useFonts,
  Jost_400Regular,
  Jost_600SemiBold
} from '@expo-google-fonts/jost'
import * as Notification from 'expo-notifications';

import Routes from './src/routes'
import { PlantsProps } from './src/libs/storage';

export default function App() {

  const [fontsLoaded] = useFonts({
    Jost_400Regular,
    Jost_600SemiBold
  });

  useEffect(() => {
    const subscription = Notification.addNotificationReceivedListener(
      async notification => {
        const data = notification.request.content.data.plant as PlantsProps;
        console.log(data)
      });

      return () => subscription.remove();


    // async function notifications() {

    //   await Notification.cancelAllScheduledNotificationsAsync();

    //   // const data = await Notification.getAllScheduledNotificationsAsync();
    //   // console.log("NOTIFICAÇÕES AGENDADAS ##################")
    //   // console.log(data)
    // }

    // notifications();
  }, [])


  if (!fontsLoaded)
    return <AppLoading />

  return (
    <Routes />
  );
}