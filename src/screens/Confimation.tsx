import { useNavigation, useRoute } from '@react-navigation/native';
import React from 'react';
import {
    StyleSheet,
    Text,
    SafeAreaView,
    View,
    KeyboardAvoidingView,
    Platform,
    TouchableWithoutFeedback,
    Keyboard
} from 'react-native';

import { Button } from '../components/Button';
import colors from '../styles/colors';
import fonts from '../styles/fonts';

interface Params {
    title: string,
    subtitle: string,
    buttonTitle: string,
    icon: 'smile' | 'hug',
    nextScreen: string
}

const emojis = {
    hug: '🤗',
    smile: '😃'
}

export function Confirmation() {

    const navigate = useNavigation()
    const routes = useRoute();

    const {
        title,
        subtitle,
        buttonTitle,
        icon,
        nextScreen
    } = routes.params as Params;

    function handleMoveOn() {
        navigate.navigate(nextScreen)
    }

    return (
        <SafeAreaView style={styles.container}>
            <KeyboardAvoidingView
                style={styles.container}
                behavior={Platform.OS === 'ios' ? 'padding' : 'height'}
            >
                <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
                    <View style={styles.content}>
                        <View style={styles.form}>
                            <Text style={styles.emoji}>
                                {emojis[icon]}
                            </Text>
                            <Text style={styles.title}>
                                {title}
                            </Text>
                            <Text style={styles.subtitle}>
                                {subtitle}
                            </Text>
                            <View style={styles.footer}>
                                <Button
                                    title={buttonTitle}
                                    onPress={handleMoveOn}
                                />
                            </View>
                        </View>
                    </View>
                </TouchableWithoutFeedback>
            </KeyboardAvoidingView>
        </SafeAreaView>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        width: '100%',
        alignItems: 'center',
        justifyContent: 'space-around',
    },
    content: {
        flex: 1,
        width: '100%'
    },
    form: {
        flex: 1,
        width: '100%',
        alignItems: 'center',
        paddingHorizontal: 54,
        justifyContent: 'center',
    },
    emoji: {
        fontSize: 44,
        marginBottom: 30
    },
    title: {
        fontSize: 32,
        textAlign: 'center',
        color: colors.heading,
        fontFamily: fonts.heading,
        lineHeight: 38,
        marginBottom: 20

    },
    subtitle: {
        textAlign: 'center',
        fontSize: 16,
        color: colors.heading,
        fontFamily: fonts.complement,
    },
    footer: {
        marginTop: 40
    }
});
