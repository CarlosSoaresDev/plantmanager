import React, { useEffect, useState } from 'react';
import {
    StyleSheet,
    Text,
    View,
    Image,
    FlatList,
    Alert
} from 'react-native';
import { formatDistance } from 'date-fns';
import { pt } from 'date-fns/locale';

import waterdrop from '../assets/waterdrop.png'
import colors from '../styles/colors';
import fonts from '../styles/fonts';
import { loadPlant, PlantsProps, removePlants } from '../libs/storage';
import { Header } from '../components/Header';
import { PlantCardSegundary } from '../components/PlantCardSegundary';
import { Load } from '../components/Load';



export function MyPlants() {
    const [myPlants, setMyPlants] = useState<PlantsProps[]>([]);
    const [loading, setLoading] = useState(true);
    const [nextWatered, setNextWatered] = useState<string>();

    useEffect(() => {
        async function loadStorageData() {

            const plantsStorage = await loadPlant();

            const nextTime = formatDistance(
                new Date(plantsStorage[0].dateTimeNotification).getTime(),
                new Date().getTime(),
                { locale: pt }
            );

            setNextWatered(
                `Não esqueça de regar a  ${plantsStorage[0].name} á ${nextTime} horas`
            )

            setMyPlants(plantsStorage);
            setLoading(false);
        }

        loadStorageData();
    })

    function handleRemove(plant: PlantsProps) {
        Alert.alert('Remover', `Deseja remover a ${plant.name}`, [
            {
                text: 'Não 🤞',
                style: 'cancel'
            },
            {
                text: 'Sim 😱',
                onPress: async () => {
                    try {

                        await removePlants(plant.id);

                        setMyPlants((oldData) => (
                            oldData.filter((item) => item.id !== plant.id)
                        ));

                    } catch (error) {
                        Alert.alert('Não foi possivel remover 😱')
                    }
                }
            },


        ])
    }

    if (loading)
        return <Load />

    return (

        <View style={styles.container}>
            <Header />
            <View style={styles.spotligth}>
                <Image
                    source={waterdrop}
                    style={styles.spotligthImage} />

                <Text style={styles.spotligthText}>
                    {nextWatered}
                </Text>
            </View>
            <View style={styles.plants}>
                <Text style={styles.plantsTitle}>
                    Proximas regadas
                </Text>
                <FlatList
                    data={myPlants}
                    keyExtractor={({ id }) => id.toString()}
                    renderItem={({ item }) => (
                        <PlantCardSegundary
                            data={item}
                            handleRemove={() => { handleRemove(item) }}
                        />
                    )}
                    showsVerticalScrollIndicator={false}
                >
                </FlatList>
            </View>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'space-between',
        paddingHorizontal: 20,
        paddingTop: 50,
        backgroundColor: colors.background,
    },
    spotligth: {
        backgroundColor: colors.blue_light,
        paddingHorizontal: 20,
        borderRadius: 20,
        height: 110,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center'
    },
    spotligthImage: {
        width: 60,
        height: 60
    },
    spotligthText: {
        flex: 1,
        color: colors.blue,
        paddingHorizontal: 20

    },
    plants: {
        flex: 1,
        width: '100%'
    },
    plantsTitle: {
        fontSize: 24,
        fontFamily: fonts.heading,
        marginVertical: 20
    }
});