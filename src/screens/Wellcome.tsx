import React from 'react';
import {
    StyleSheet,
    Text,
    Image,
    SafeAreaView,
    TouchableOpacity,
    Dimensions,
    View
} from 'react-native';
import { Entypo } from '@expo/vector-icons';
import { useNavigation } from '@react-navigation/core';

import colors from '../styles/colors';
import fonts from '../styles/fonts'
import wateringImg from '../assets/watering.png'

export function Wellcome() {

    const navigate = useNavigation()

    function handleStart() {
        navigate.navigate('UserIdentification')
    }

    return (
        <SafeAreaView style={styles.container}>
            <View style={styles.wrapper}>
                <Text style={styles.title}>
                    Gerencie {'\n'}
                suas plantas {'\n'}
                de forma facil {'\n'}
                </Text>
                <Image
                    style={styles.image}
                    source={wateringImg}
                    resizeMode="contain" />
                <Text style={styles.subtitle}>
                    Não esqueça de regar suas plantas.
                    Nós lembramos á você sempre que precisar.
                </Text>
                <TouchableOpacity
                    style={styles.button}
                    activeOpacity={0.7}
                    onPress={handleStart} >
                    <Text>
                        <Entypo style={styles.buttonIcon} name="chevron-right" />
                    </Text>
                </TouchableOpacity>
            </View>
        </SafeAreaView>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    wrapper: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'space-around',
        paddingHorizontal: 20
    },
    title: {
        fontSize: 28,
        fontFamily: fonts.heading,
        fontWeight: 'bold',
        textAlign: 'center',
        color: colors.heading,
        marginTop: 38,
        lineHeight: 34
    },
    subtitle: {
        fontSize: 18,
        fontFamily: fonts.text,
        textAlign: 'center',
        paddingHorizontal: 28,
        color: colors.heading,
    },
    image: {
        height: Dimensions.get('window').width * 0.7
    },
    button: {
        backgroundColor: colors.green,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 16,
        marginBottom: 10,
        height: 56,
        width: 56
    },
    buttonIcon: {
        color: colors.white,
        fontSize: 32
    }
});
