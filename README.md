## Intalações

expo init <nome-do-projeto>

## npm install expo-font 
Comando para instalar pacotes referente ao font da aplicações

## npm install @expo/vector-icons
Comando para instalar um pacotes de icons
[https://www.npmjs.com/package/@expo/vector-icons/](Site)

## npm install expo-font 
Comando para instalar pacotes referente ao font da aplicações
[https://docs.expo.io/guides/using-custom-fonts/](Site)

## npm install @expo-google-fonts/<o-nome-da-font>
Comando para instalar pacotes referente ao font da aplicações do google
[https://docs.expo.io/guides/using-custom-fonts/](Site)
[https://fonts.google.com/specimen/Jost?query=jost/](Site)


## npm install expo-app-loading
Comando para instalar pacotes para lazy load nativo no expo
[https://docs.expo.io/guides/using-custom-fonts/](Site)
[https://fonts.google.com/specimen/Jost?query=jost/](Site)


## npm install @react-navigation/native
Comando para instalar pacotes React Navigator para navegação de paginas
[https://reactnavigation.org/docs/getting-started/](Site)


## expo install react-native-gesture-handler react-native-reanimated react-native-screens react-native-safe-area-context @react-native-community/masked-view
Comando para instalar pacotes Outras dependecias para a navegação mais nativa
[https://reactnavigation.org/docs/getting-started/](Site)


## npm install @react-navigation/stack
Comando para instalar pacotes Para pilhas de navegação
[https://reactnavigation.org/docs/hello-react-navigation](Site)



## npm install react-hook-form
Comando para instalar pacotes para manipulação de formulario
[https://reactnavigation.org/docs/hello-react-navigation](Site)


## npm install react-native-status-bar-height
Comando para instalar pacotes manipulação de dimensao da tela
[https://www.npmjs.com/package/@freakycoder/react-native-helpers](Site)

## npm install axios
Comando para instalar pacotes http client para fazer requisições
[https://www.npmjs.com/package/@freakycoder/react-native-helpers](Site)


## npm install -g json-server
Comando para instalar pacotes para simular um servidor instalação global
[https://www.npmjs.com/package/@freakycoder/react-native-helpers](Site)

Rodar o json serve comando -> json-server ./src/services/server.json --host <SeuIp> --port 3333

## expo install react-native-svg
Comando para instalar pacotes para poder aplicar svg na aplicação com expo
[https://docs.expo.io/versions/latest/sdk/svg/](Site)

Rodar o json serve comando -> json-server ./src/services/server.json --host <SeuIp> --port 3333 --dalay 700


Site para load
[https://lottiefiles.com/search?q=plant&category=animations](Site)


## expo install lottie-react-native
Comando para instalar pacotes para poder  mexer com load lottie
[https://docs.expo.io/versions/latest/sdk/svg/](Site)


## expo install @react-native-async-storage/async-storage
Comando para instalar pacotes para poder Salvar dados em memoria local
[https://docs.expo.io/versions/latest/sdk/async-storage/](Site)


## expo install @react-native-community/datetimepicker
Comando para instalar pacotes para poder trabalhar com manipulação de datas
[https://docs.expo.io/versions/latest/sdk/async-storage/](Site)

## npm install date-fns --save
Comando para instalar pacotes para poder trabalhar com manipulação de datas de melhor formas
[https://date-fns.org/docs/Getting-Started](Site)


## npm install @react-navigation/bottom-tabs
Comando para instalar pacotes para poder trabalhar com tab navigação
[https://reactnavigation.org/docs/tab-based-navigation](Site)



## expo install expo-notifications
Comando para instalar pacotes para poder trabalhar Com notificações
[https://docs.expo.io/versions/latest/sdk/notifications/](Site)



## expo build:android -t apk
Comando para criar o apk para sistemas androids
[https://docs.expo.io/versions/latest/sdk/notifications/](Site)