import React, { useState, useEffect } from 'react';
import { useNavigation } from '@react-navigation/native';
import { useForm } from 'react-hook-form'
import {
    StyleSheet,
    Text,
    SafeAreaView,
    View,
    TextInput,
    KeyboardAvoidingView,
    Platform,
    Alert
} from 'react-native';

import AsyncStorage from '@react-native-async-storage/async-storage'


import { Button } from '../components/Button';
import colors from '../styles/colors';
import fonts from '../styles/fonts';

export function UserIdentification() {

    const [isFocused, setIsFocused] = useState(false);
    const [isFilled, setIsFilled] = useState(false);
    const [name, setName] = useState<string>();
    const { register, setValue, handleSubmit } = useForm()
    const navigate = useNavigation()

    function handleInputBlur() {
        setIsFocused(false)
        setIsFilled(!!name)
    }

    function handleInputFocus() {
        setIsFocused(true)
    }



    useEffect(() => {
        register('name')
    }, [register])


    async function onSubmit(data: any) {
        if (!data.name)
            return Alert.alert('Me diz como chama você 🦄 ');

        try {
            await AsyncStorage.setItem('@plantmanager:user', data.name);

            navigate.navigate('Confirmation', {
                title: 'Prontinho',
                subtitle: 'Agora vamos começar a cuidar das suas plantinhas com muito cuidado',
                buttonTitle: 'Começar',
                icon: 'smile',
                nextScreen: 'PlantSelect'
            });

        } catch {
            return Alert.alert('Não foi possivel salvar seu nome 😯');
        }
    }

    return (
        <SafeAreaView style={styles.container}>
            <KeyboardAvoidingView
                style={styles.container}
                behavior={Platform.OS === 'ios' ? 'padding' : 'height'}
            >
                <View style={styles.content}>
                    <View style={styles.form}>

                        <Text style={styles.emoji}>
                            {isFilled ? '😀' : '🙂'}
                        </Text>
                        <Text style={styles.title}>
                            Como podemos {'\n'}
                            chamar você?
                        </Text>
                        <TextInput
                            style={[
                                styles.input,
                                (isFocused || isFilled) && { borderColor: colors.green }
                            ]}
                            placeholder="Digite seu nome"
                            onBlur={handleInputBlur}
                            onFocus={handleInputFocus}
                            onChangeText={text => {
                                setValue('name', text);
                                setIsFilled(!!text);
                                setName(text);
                            }}
                        />
                        <View style={styles.footer}>
                            <Button
                                title="Confirmar"
                                onPress={handleSubmit(onSubmit)}
                            />
                        </View>
                    </View>
                </View>
            </KeyboardAvoidingView>
        </SafeAreaView>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        width: '100%',
        alignItems: 'center',
        justifyContent: 'space-around',
    },
    content: {
        flex: 1,
        width: '100%'
    },
    form: {
        flex: 1,
        width: '100%',
        alignItems: 'center',
        paddingHorizontal: 54,
        justifyContent: 'center',
    },
    emoji: {
        fontSize: 44,
        marginBottom: 30

    },
    input: {
        borderBottomWidth: 1,
        borderColor: colors.gray,
        color: colors.heading,
        width: '100%',
        fontSize: 18,
        padding: 10,
        textAlign: 'center'
    },
    title: {
        fontSize: 32,
        textAlign: 'center',
        color: colors.heading,
        fontFamily: fonts.heading,
        lineHeight: 38,
        marginBottom: 40

    },
    footer: {
        marginTop: 40
    }
});
