import React, { useEffect, useState } from 'react';
import {
    StyleSheet,
    View,
    Text,
    TouchableOpacity,
    Image
} from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage'

import colors from '../styles/colors';
import fonts from '../styles/fonts'
import userImage from '../assets/profile_i.jpg'

export function Header() {

    const [userName, setUserName] = useState<string>();

    useEffect(() => {

        async function loadStorage() {
            const name = await AsyncStorage.getItem('@plantmanager:user');
            setUserName(name || '');
        }

        loadStorage();
    }, [])

    return (
        <View style={styles.container}>
            <View>
                <Text style={styles.greeting}>
                    Olá,
                </Text>
                <Text style={styles.userName}>
                    {userName}
                </Text>
            </View>
            <TouchableOpacity activeOpacity={0.7}>
                <Image
                    source={userImage}
                    style={styles.image}
                />
            </TouchableOpacity>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        width: '100%',
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        paddingVertical: 20,
        marginTop: 20,
    },
    image: {
        width: 70,
        height: 70,
        borderRadius: 35
    },
    greeting: {
        color: colors.heading,
        fontSize: 30,
        fontFamily: fonts.text
    },
    userName: {
        color: colors.heading,
        fontSize: 32,
        fontFamily: fonts.heading,
        lineHeight: 40
    }
});